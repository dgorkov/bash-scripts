#!/bin/bash


### Переменные

  BACKUP_DIR=/2TB_HDD/sql_backups/production/ibolit_prod

  DATABASES=(ibolit_prod)
  USER=sqlbackup
  PASSWORD=pEDFN238MOxJWS3XD3Qf
  HOST=10.10.172.112
  MYSQLPORT=3306

  ERRORLOG=/2TB_HDD/sql_backups/error.log
  SUCCESSLOG=/2TB_HDD/sql_backups/success.log

  HOURLY_ROTATION_TIME=3 # Задаём время в днях, старше которого бекапы будут удалены из ежечасной директории
  DAILY_ROTATION_TIME=10 # Задаём время в днях, старше которого бекапы будут удалены из ежедневной директории


### Резервирование и ротация

  if [[ "${1}" == "hourly" ]]; then 
  
    if nc -z ${HOST} ${MYSQLPORT} 2>/dev/null; then
      
      for base in ${DATABASES[@]};
        do mysqldump -u ${USER} -p${PASSWORD} -h ${HOST} --events --routines --triggers --single-transaction ${base} | gzip > ${BACKUP_DIR}/${1}/`date +%Y-%m-%d_%H-%M`_${base}.sql.gz;
      done
      find ${BACKUP_DIR}/${1}/ -type f -mtime +$HOURLY_ROTATION_TIME -exec rm {} +
      echo "`date +%Y-%m-%d` `date +%H:%M` Ежечасное резервирование и очистка успешно завершены" | tee -a ${SUCCESSLOG}

    else

      echo "`date +%Y-%m-%d` `date +%H:%M` Порт ${MYSQLPORT} недоступен на сервере ${HOST}" | tee -a ${ERRORLOG}
      exit 1

    fi


  elif [[ "${1}" == "daily" ]]; then

    if nc -z ${HOST} ${MYSQLPORT} 2>/dev/null; then

      mysqldump -u ${USER} -p${PASSWORD} -h ${HOST} --events --routines --triggers --single-transaction --all-databases | gzip > ${BACKUP_DIR}/${1}/`date +%Y-%m-%d_%H-%M`_all.sql.gz;
      find ${BACKUP_DIR}/${1}/ -type f -mtime +$DAILY_ROTATION_TIME -exec rm {} +
      echo "`date +%Y-%m-%d` `date +%H:%M` Ежедневное резервирование и очистка успешно завершены" | tee -a ${SUCCESSLOG}

    else

      echo "`date +%Y-%m-%d` `date +%H:%M` Порт ${MYSQLPORT} недоступен на сервере ${HOST}" | tee -a ${ERRORLOG}
      exit 1

    fi


  else

      echo "`date +%Y-%m-%d` `date +%H:%M` Не указана корректная директория резервирования - daily или hourly" | tee -a ${ERRORLOG}
      exit 1

  fi


exit 0
