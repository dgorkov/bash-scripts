#!/bin/bash


### Переменные


  TYPE=$1
  DATABASE=$2
  HOST=$3
  USER=sqlbackup
  PASSWORD=pEDFN238MOxJWS3XD3Qf
  MYSQLPORT=3306

  BACKUP_DIR=/2TB_HDD/sql_backups/${TYPE}/${DATABASE}

  ERRORLOG=/2TB_HDD/sql_backups/error.log
  SUCCESSLOG=/2TB_HDD/sql_backups/success.log

  ROTATION_TIME=7 # Задаём время в днях, старше которого бекапы будут удалены


### Резервирование и ротация
  
    if nc -z ${HOST} ${MYSQLPORT} 2>/dev/null; then
      
      mysqldump -u ${USER} -p${PASSWORD} -h ${HOST} --events --routines --triggers --single-transaction ${DATABASE} | gzip > ${BACKUP_DIR}/`date +%Y-%m-%d_%H-%M`_${DATABASE}.sql.gz;
      find ${BACKUP_DIR}/ -type f -mtime +$ROTATION_TIME -exec rm {} +
      echo "`date +%Y-%m-%d` `date +%H:%M` Резервирование базы ${DATABASE} и очистка успешно завершены" | tee -a ${SUCCESSLOG}

    else

      echo "`date +%Y-%m-%d` `date +%H:%M` Порт ${MYSQLPORT} недоступен на сервере ${HOST}" | tee -a ${ERRORLOG}
      exit 1

    fi

exit 0
