#!/bin/sh
 
SDIR="/storage_hdd"
SKEY="/root/.ssh/id_rsa"
SLOG="$SDIR/backup.log"
PID_FILE="$SDIR/backup.pid"
ADMIN_EMAIL="dgorkov@ibolit.pro"
 
 
if [ -e $PID_FILE ]; then
        echo "Эта задача уже выполняется или предыдущий запуск был завершен с ошибками на `hostname`" | mail -s "Какая-то неразбериха с бэкапами на `hostname`..." $ADMIN_EMAIL
        exit
fi
 
touch $PID_FILE
 
# перенаправление всего вывода в файл журнала
exec >> $SLOG 2>&1
 
# разбор файла backup.ini на переменные  $from variables
cat backup.ini | while read domain from ; do
	destination="astra-kvm-node-2"
    domain="astra-kvm-node-2"
	# загрузка свежей копии в 'latest' каталог
	echo -e "`date` *** $domain резервное копирование началось">>$SLOG
 
	# начать подсчет рабочего времени rsync
	start=$(date +%s)
	rsync --archive --one-file-system --delete -e "ssh -i $SKEY" "$from" "$destination/latest" || (echo -e "Error when rsyncing $domain. \n\n For more information see $SLOG:\n\n `tail $SLOG`" | mail -s "rsync error" $ADMIN_EMAIL & continue)
	finish=$(date +%s)
	echo -e "`date` *** RSYNC работал в период $((finish - start)) секунды">>$SLOG
 
    # клонирование свежей копии с помощью жесткой ссылки
	cp --archive --link "$destination/latest" "$destination/`date +%F`"
	# удаление всех предыдущих копий старше 7 дней по дате создания, но не'latest'
	find "$destination" -maxdepth 1 -ctime +7 -type d -path "$destination/????-??-??" -exec rm -r -f {} \;
	echo "`date` *** Размер $domain/latest сейчас `du -sh $destination/latest | awk '{print $1}'` ">>$SLOG
	echo -e "`date` *** $domain резервное копирование завершено">>$SLOG
	echo -e "`date` *** Всего выделено `du -sh $destination | awk '{print $1}'`">>$SLOG
	echo -e "------------------------------------------------------------------">>$SLOG
done
 
rm $PID_FILE