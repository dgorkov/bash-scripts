#!/bin/bash
#
#
# Change external IP office_msk in file /etc/iptables.sh and execute him for firewall rules update
#
#
#----------------------------------------------------------------------------------------------------------

ext_ip_office_in_iptables=$(cat /etc/iptables.sh | grep "export msk_office_ip=" | awk -F "=" '{print $2}')

real_ext_ip_office=$(tail -n 1 /var/log/nginx/other_services/access_____temp.find.office.ip.ibolit.pro.log)

#----------------------------------------------------------------------------------------------------------




if [ "$ext_ip_office_in_iptables" != "$real_ext_ip_office" ]; then

cp /etc/iptables.sh /opt/backups_of_important_files/iptables/iptables.sh___from___$(date "+%d-%m-%Y-----%H-%M")

chmod 600 /opt/backups_of_important_files/iptables/*

sed -i "s/.*export msk_office_ip=.*/export msk_office_ip=$real_ext_ip_office/g" /etc/iptables.sh

sudo /etc/iptables.sh

else

echo "IP addresses match, nothing needs to be done"

fi



#----------------------------------------------------------------------------------------------------------


# Clear IP addresses log file if it has more than 600 lines

log_size=$(cat /var/log/nginx/other_services/access_____temp.find.office.ip.ibolit.pro.log | wc -l)

if [ $log_size -gt 44640 ]; then

echo $real_ext_ip_office > /var/log/nginx/other_services/access_____temp.find.office.ip.ibolit.pro.log

fi
