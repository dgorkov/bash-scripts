#!/bin/bash
#
# Объявление переменных
export IPT="iptables"

# Внешние сетевые интерфейсы
export WAN=vmbr0
export WAN_IP=88.212.236.244

# Внутрение сетевые интерфейсы
export LAN1=vmbr1
export INT_LAN_IP=10.181.195.132
export LAN1_IP_RANGE=10.10.172.0/24

export LAN2=vmbr1:1
export LAN2_IP_RANGE=10.10.172.0/24

# Интерфейсы OpenVPN
export VPN=tun0
export VPN_IP_RANGE=10.10.40.0/25
export ALL_VPN_RANGE=10.10.30.0/25,10.10.40.0/25,10.10.41.0/25,10.10.42.0/25

# Внешние разрешенные IP адреса (перечисляются через запятую, без пробелов)
export msk_office_ip=89.175.14.197
export ibolit_balancer=23.105.248.137,10.181.194.67
export ibolit_balancer_internal=10.181.194.67
export ibolit_traefik=23.111.103.232,10.181.194.70
export external_backup_host=51.159.34.58

export kvm_node_2=88.212.237.148,10.181.194.132,10.10.172.42

# Внешние Zabbix агенты
export external_zabbix_agents=23.111.98.167,51.159.34.58

# Очистка всех цепочек iptables
$IPT -F
$IPT -F -t nat
$IPT -F -t mangle
$IPT -X
$IPT -t nat -X
$IPT -t mangle -X

# Установим политики по умолчанию для трафика, не соответствующего ни одному из правил
$IPT -P INPUT DROP
$IPT -P OUTPUT DROP
$IPT -P FORWARD DROP

# разрешаем loopback, локалку, и WAN
$IPT -A INPUT -i lo -j ACCEPT
$IPT -A INPUT -i $LAN1 -j ACCEPT
$IPT -A INPUT -i $LAN2 -j ACCEPT
$IPT -A INPUT -i $VPN -j ACCEPT

$IPT -A OUTPUT -o lo -j ACCEPT
$IPT -A OUTPUT -o $WAN -j ACCEPT
$IPT -A OUTPUT -o $LAN1 -j ACCEPT
$IPT -A OUTPUT -o $LAN2 -j ACCEPT

# разрешаем выход с виртуалок в интернет
$IPT -A FORWARD -s $LAN1_IP_RANGE -o $WAN -j ACCEPT
$IPT -t nat -A POSTROUTING -s $LAN1_IP_RANGE -o $WAN -j MASQUERADE

# Правило позволяет виртуальным машинам обращаться к внутренним DNS серверам servers.ru
$IPT -t nat -A POSTROUTING -s $LAN1_IP_RANGE -d 192.168.8.8,192.168.8.88 -j MASQUERADE

# Правило позволяет виртуальным машинам обращаться к внутренним DNS серверам servers.ru
$IPT -t nat -A POSTROUTING -s $LAN2_IP_RANGE -d 188.42.211.11 -j MASQUERADE

# Правило разрешает трафику приходящему с адресов 10.10.172.0/24 обращатся на текущий сервер и его виртуальные машины.
$IPT -A FORWARD -s $LAN1_IP_RANGE -o $LAN1 -j ACCEPT
$IPT -A FORWARD -s $LAN2_IP_RANGE -o $LAN2 -j ACCEPT

# Правило разрешает пользователям OpenVPN подключаться к машинам внутри контура
$IPT -A FORWARD -s $ALL_VPN_RANGE -d $LAN1_IP_RANGE -j ACCEPT
$IPT -A FORWARD -s $ALL_VPN_RANGE -d 172.30.30.0/23 -j ACCEPT

# Правило разрешает проброс подключения к динамическим стендам через ibolit-balancer
$IPT -t nat -A POSTROUTING -s $ALL_VPN_RANGE -d 23.105.248.137 -j MASQUERADE
$IPT -A FORWARD -s $ALL_VPN_RANGE -d 23.105.248.137 -j ACCEPT

# Правило разрешает проброс подключения к backup-host
$IPT -t nat -A POSTROUTING -s $ALL_VPN_RANGE -d 51.159.34.58 -j MASQUERADE
$IPT -A FORWARD -s $ALL_VPN_RANGE -d 51.159.34.58 -j ACCEPT


##########################
#        I P S e c       #
##########################


# проброс до IPSec сервера 
#$IPT -I INPUT -i $WAN -s $msk_office_ip -p udp  --dport 500 -j ACCEPT
#$IPT -I INPUT -i $WAN -s $msk_office_ip -p udp  --dport 4500 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $WAN -s $msk_office_ip -p udp --dport 500 -j DNAT --to 10.10.172.254:500
#$IPT -A FORWARD -i $WAN -o $LAN1 -d 10.10.172.254 -p udp --dport 500 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $WAN -s $msk_office_ip -p udp --dport 4500 -j DNAT --to 10.10.172.254:4500
#$IPT -A FORWARD -i $WAN -o $LAN1 -d 10.10.172.254 -p udp --dport 4500 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $WAN -s $msk_office_ip -p tcp --dport 4500 -j DNAT --to 10.10.172.254:4500
#$IPT -A FORWARD -i $WAN -o $LAN1 -d 10.10.172.254 -p tcp --dport 4500 -j ACCEPT

##########################
#    Всякие настройки    #
##########################


# разрешаем пинги
$IPT -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

# Состояние ESTABLISHED говорит о том, что это не первый пакет в соединении.
# Пропускать все уже инициированные соединения, а также дочерние от них
$IPT -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
# Пропускать новые, а так же уже инициированные и их дочерние соединения
$IPT -A OUTPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
# Разрешить форвардинг для уже инициированных и их дочерних соединений
$IPT -A FORWARD -p all -m state --state ESTABLISHED,RELATED -j ACCEPT

# Включаем фрагментацию пакетов. Необходимо из за разных значений MTU
$IPT -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

# Отбрасывать все пакеты, которые не могут быть идентифицированы
# и поэтому не могут иметь определенного статуса.
$IPT -A INPUT -m state --state INVALID -j DROP
$IPT -A FORWARD -m state --state INVALID -j DROP

# Приводит к связыванию системных ресурсов, так что реальный
# обмен данными становится не возможным, обрубаем
$IPT -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
$IPT -A OUTPUT -p tcp ! --syn -m state --state NEW -j DROP


################################################
#     Разрешаем входящие соединения из LAN     #
################################################


$IPT -A INPUT -i $LAN1 -j ACCEPT
$IPT -A FORWARD -i $LAN1 -j ACCEPT


##########################
#     ВНЕШНИЙ ДОСТУП     #
##########################

# Разрешаем подключения конкретных Zabbix агентов из вне к Zabbix Proxy.
$IPT -A INPUT -i $WAN -s $external_zabbix_agents -p tcp --dport 10051 -j ACCEPT

# TCP port for syslog messages transfer to graylog server from balancer (syslog and gelf)
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer_internal,$ibolit_traefik -p tcp --dport 1514 -j ACCEPT
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer_internal,$ibolit_traefik -p tcp --dport 1515 -j ACCEPT

#--------------------------------------
# TCP ports for access to prod-influx-1

$IPT -A INPUT -i $LAN1 -s 10.181.194.70 -p tcp --dport 1526 -j ACCEPT
$IPT -A INPUT -i $LAN1 -s 10.181.194.70 -p tcp --dport 1528 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $LAN1 -s 10.181.194.70 -p tcp --dport 1526 -j DNAT --to 10.10.172.71:8086
#$IPT -A FORWARD -i $LAN1 -o $LAN1 -d 10.10.172.71 -p tcp --dport 1526 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $LAN1 -s 10.181.194.70 -p tcp --dport 1528 -j DNAT --to 10.10.172.71:8088
#$IPT -A FORWARD -i $LAN1 -o $LAN1 -d 10.10.172.71 -p tcp --dport 1528 -j ACCEPT
#--------------------------------------

#--------------------------------------
# TCP ports for access to prod-portainer from SWARM

$IPT -A INPUT -i $LAN1 -s $ibolit_traefik -p tcp --dport 1538 -j ACCEPT
$IPT -A INPUT -i $LAN1 -s $ibolit_traefik -p tcp --dport 1539 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $LAN1 -s $ibolit_traefik -p tcp --dport 1538 -j DNAT --to 10.10.172.130:8000
#$IPT -A FORWARD -i $LAN1 -o $LAN1 -d 10.10.172.130 -p tcp --dport 1538 -j ACCEPT

#$IPT -t nat -A PREROUTING -i $LAN1 -s $ibolit_traefik -p tcp --dport 1539 -j DNAT --to 10.10.172.130:9000
#$IPT -A FORWARD -i $LAN1 -o $LAN1 -d 10.10.172.130 -p tcp --dport 1539 -j ACCEPT
#--------------------------------------



# Websocket from balancer for production Laravel Echo Server
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer_internal -p tcp --dport 6001 -j ACCEPT

# WebSockets from balancer for Nginx-Dynamic-Domains (4000-4100 MCS; 7000-7100 Laravel)
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer_internal -p tcp --dport 7000:7100 -j ACCEPT
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer_internal -p tcp --dport 4000:4100 -j ACCEPT

# Открываем 873 порт на котором работает rsyncd для доступа с backup хоста
$IPT -A INPUT -i $WAN -s $external_backup_host -p tcp --dport 873 -j ACCEPT

# Открываем порт для DNS
$IPT -A INPUT -i $WAN -p udp --dport 53 -j ACCEPT

# Открываем порт для NTP
$IPT -A INPUT -i $WAN -p udp --dport 123 -j ACCEPT
$IPT -A INPUT -i $LAN1 -p udp --dport 123 -j ACCEPT

# Открываем порт для Nginx
$IPT -A INPUT -i $WAN -p tcp --dport 80 -j ACCEPT
$IPT -A INPUT -i $WAN -p tcp --dport 443 -j ACCEPT

# Разрешаем подключение к OpenVPN
$IPT -A INPUT -i $WAN -p udp --dport 1195 -j ACCEPT

# Разрешаем поключение к WEB морде Proxmox
#$IPT -A INPUT -i $WAN -p tcp --dport 8006 -j ACCEPT

# Разрешаем поключение к серверу Proxmox через SSH
$IPT -A INPUT -i $WAN -p tcp --dport 2222 -j ACCEPT

# Разрешаем поключение к TCP порту 13306 который слушает Nginx (для проброса на MySQL)
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer,$ibolit_traefik -p tcp --dport 13306 -j ACCEPT

# Разрешаем поключение к TCP порту 6001 который слушает Nginx (для проброса на laravel-echo-server)
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer -p tcp --dport 16001 -j ACCEPT

# Разрешаем подключение к TCP порту 16379 (для проброса к машине prod-redis)
$IPT -A INPUT -i $LAN1 -s $ibolit_balancer,$ibolit_traefik -p tcp --dport 16379 -j ACCEPT

# Разрешаем подключение к UDP порту 2049 для монтирования сетевой NFS папки на машину ibolit-traefik
$IPT -A INPUT -i $LAN1 -s $ibolit_traefik -p udp --dport 2049 -j ACCEPT

##########################
#         Л О Г И        #
##########################

# Для включение логирование расскоментируйте следующие строки:
#$IPT -N block_in
#$IPT -N block_out
#$IPT -N block_fw
#$IPT -A INPUT -j block_in
#$IPT -A OUTPUT -j block_out
#$IPT -A FORWARD -j block_fw
#$IPT -A block_in -j LOG --log-level info --log-prefix "--IN--BLOCK"
#$IPT -A block_in -j DROP
#$IPT -A block_out -j LOG --log-level info --log-prefix "--OUT--BLOCK"
#$IPT -A block_out -j DROP
#$IPT -A block_fw -j LOG --log-level info --log-prefix "--FW--BLOCK"
#$IPT -A block_fw -j DROP


# Записываем правила в файл
/sbin/iptables-save > /etc/iptables_rules
